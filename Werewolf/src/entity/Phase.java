package entity;

public class Phase {
	private int day;
	private String time;
	private String[] timeCandidate;
	public Phase() {
		timeCandidate = new String[2];
		timeCandidate[0] = "day";
		timeCandidate[1] = "night";
		time = timeCandidate[0];
		day = 1;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	public void nextPhase() {
		if(time.equals(timeCandidate[0])) {
			time = timeCandidate[1];
		} else {
			time = timeCandidate[0];
			day++;
		}
	}
}
