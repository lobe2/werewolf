package entity;

public class KPU {
	private int id;
	private int num_vote;

	public KPU() {
		id = -1;
		num_vote = 0;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNum_vote() {
		return num_vote;
	}
	public void setNum_vote(int num_vote) {
		this.num_vote = num_vote;
	}
}
