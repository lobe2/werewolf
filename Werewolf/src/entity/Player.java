package entity;
/**
 * 
 * Nama : Muhtar H
 * NIM : 13513068
 * 
 * */
public class Player {
	
	private String udp_addr;
	private int udp_port;
	private String username;
	private int isAlive;
	private int playerId;
	private String role;
	
	public Player() {
		setUdpAddr("0.0.0.0");
		setUdpPort(0);
		setUsername("undef");
		setIsAlive(1);
		setPlayerId(-1);
		setRole("none");
	}
	public Player(int id, String IP, int port, String uname, int alive) {
		setUdpAddr(IP);
		setUdpPort(port);
		setUsername(uname);
		setIsAlive(alive);
		setPlayerId(id);
		setRole("none");
	}
	
	
	/**
	 * GETTER and SETTER
	 * */
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIsAlive() {
		return isAlive;
	}
	public void setIsAlive(int isAlive) {
		this.isAlive = isAlive;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public String getUdpAddr() {
		return udp_addr;
	}
	public void setUdpAddr(String udp_addr) {
		this.udp_addr = udp_addr;
	}
	public int getUdpPort() {
		return udp_port;
	}
	public void setUdpPort(int udp_port) {
		this.udp_port = udp_port;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		String S = "{\"player_id\" : " 
					+ (new Integer(getPlayerId())).toString() 
					+ ", " 
					+ "\"is_alive\" : " 
					+ (new Integer(isAlive)).toString() 
					+ ", "
					+ "\"address\" : " 
					+ "\""+ udp_addr + "\"" 
					+ ", "
					+ "\"port\" : " 
					+ (new Integer(udp_port)).toString()
					+ ", " 
					+ "\"username\" : "
					+ "\""+ username + "\""
					//+ ", "
					//+ "\"role\" : "
					//+ "\""+ role + "\""
					+ "}";
		return S;
	}
}
