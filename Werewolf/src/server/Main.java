package server;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class Main {
	
	public static void main(String args[]) {
		int PORT = 9876;
		//setver ip address
		
		try {
			System.out.println(Inet4Address.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ServerSocket ss;
		try {
			ss = new ServerSocket(PORT);
			ss.setReuseAddress(true);
			Server server = new Server(ss);
			server.start();
			server.play();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
