package server;

import java.util.ArrayList;

public class ResponseContract {
	private static final String failMethod = "\"status\" : \"fail\"";
	private static final String startMethod = "\"method\" : \"start\"";
	private static final String changePhaseMethod = "\"method\" : \"change_phase\"";
	private static final String okMethod = "\"status\" : \"ok\"";
	private static final String errorMethod = "\"status\" : \"error\"";
	private static final String vote_now = "\"method\" : \"vote_now\"";
	private static final String gameOverMethod = "\"method\" : \"game_over\"";
	private static final String playerKilledMethod = "\"method\" : \"player_killed\"";
	
	public static String failResponse(String description) {
		String response = "{" + failMethod + "," +
							"\"description\" : \"" + 
							description + "\"}";
		return response;
	}
	public static String okJoin(int id) {
		String S = "{" + okMethod + "," +
				"\"player_id\" : " + 
				(new Integer(id)).toString() + "}";;
		return S;
	}
	
	public static String errorResponse(String description) {
		String response = "{" + errorMethod + "," +
				"\"description\" : \"" + 
				description + "\"}";
		return response;
	}
	
	public static String okGeneral(String description) {
		String response = "{" + okMethod + "," +
				"\"description\" : \"" + 
				description + "\"}";
		return response;
	}
	
	public static String okLeave() {
		String response = "{" + okMethod + "}";
		return response;
	}
	
	public static String okReady(String description) {
		String response = "{" + okMethod + "," +
				"\"description\" : \"" + 
				description + "\"}";			
		return response;
	}
	
	public static String okReady() {
		String response = "{" + okMethod + "}";
		return response;
	}
	
	public static String getAllPlayerInfo() {
		String description = "list of clients retrieved";
		String clients = Server.playersToString();
		String response = "{" 
							+ okMethod 
							+ ", "
							+ "\"clients\" : "
							+ clients
							+ ",\"description\" : \"" 
							+ description 
							+ "\"}";
		return response;
	}
	
	public static String startGame(String role, ArrayList<String> friends) {
		String friendsArr = "[";
		for(int i = 0; i < friends.size(); i++) {
			friendsArr += "\"" + friends.get(i)+"\", ";
		}
		friendsArr = friendsArr.substring(0, friendsArr.length()-3);
		String response = "{"
							+ startMethod
							+ ", "
							+ "\"time\" : \"day\""
							+ ", "
							+ "\"role\" : "
							+ "\"" + role + "\""
							+ ", "
							+ "\"friend\" : " + friendsArr 
							+ "\"], "
							+ "\"description\" : \"game is started\""
							+ "}";
		return response;
	}
	
	public static String startGame(String role) {
		String response = "{"
				+ startMethod
				+ ", "
				+ "\"time\" : \"day\""
				+ ", "
				+ "\"role\" : "
				+ "\"" + role + "\""
				+ ", "
				+ "\"description\" : \"game is started\""
				+ "}";
		return response;
	}
	
	public static String changePhase(int day, String time) {
		String response = "{"
							+ changePhaseMethod
							+ ", "
							+ "\"time\" : "
							+ "\"" + time + "\""
							+ "\"days\" : " + (new Integer(day)).toString()
							+ "\"description\" : \"\""
							+ "}";
		return response;
	}
	
	public static String voteNotify(String phase) {
		String response = "{"
				+ vote_now
				+ ", "
				+ "\"phase\" : "
				+ "\"" + phase + "\""
				+ "}";
		return response;
	}
	
	public static String gameOver(String winner) {
		String response = "{"
				+ gameOverMethod
				+ ", "
				+ "\"winner\" : "
				+ "\"" + winner + "\""
				+ ", "
				+ "\"description\" : \"game is over\""
				+ "}";
		return response;
	}
	
	public static String playerKilled(int id, String role) {
		String idPlayer = (new Integer(id)).toString();
		String response = "{"
				+ playerKilledMethod
				+ ", "
				+ "\"id\" : "
				+ idPlayer
				+ ", "
				+ "\"role\" : "
				+ "\"" + role + "\""
				+ ", "
				+ "\"description\" : \"player killed\""
				+ "}";
		return response;
	}
}
