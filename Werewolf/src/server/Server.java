package server;

import java.io.*;
import java.net.*;
import java.util.*;

import entity.*;

public class Server extends Thread{
	private ServerSocket ss;
    
	public static Hashtable<Integer, Player> players;
    public static ArrayList<PrintWriter> writers = new ArrayList<PrintWriter>();
    public  static int numWerewolf = 2;
    public static int numCivilian = 4;
    public static boolean regis_complete = false;
    private static Integer numPlayers;
    public static boolean isPlay;
	public static KPU kpu = new KPU();
	public static boolean start = false;
    private ArrayList<Integer> wwolf = new ArrayList<Integer>();
    
    private static Phase phase;
    public static Boolean phaseChanged = true;
    
    public Server(ServerSocket ss) {
    	setPhase(new Phase());
		this.ss = ss;
		players  = new Hashtable<Integer, Player>();
		writers = new ArrayList<PrintWriter>();
		numPlayers = 0;
	}
	
    public void initPlayer() {
    	
    	while(numPlayers < 6 || !start) {
    		System.out.print("");
    	}
    	
		Random rand = new Random();
		int rand1, rand2;
		rand1 = rand.nextInt(numPlayers);
		rand2 = rand1;
				
		while(rand2 == rand1){
			rand2 = rand.nextInt(numPlayers);
		}
		
		wwolf.add(rand1);
		wwolf.add(rand2);
		for(int i = 0; i < numPlayers; i++) {
			if(i == rand1 || i == rand2) {
				players.get(i).setRole("werewolf");
			}else{
				players.get(i).setRole("civilian");
			}
		}
    }
    
	public void startServer() {
		initPlayer();
		
		//kirim role dan informasi ke setiap player
		//unreliable for loop
		for(int i = 0; i < numPlayers; i++) {
			String role = players.get(i).getRole();
			if(role.equals("werewolf")) {
				ArrayList<String> friends = new ArrayList<String>();
				for(int j = 0; j < wwolf.size(); j++) {
					if(wwolf.get(j) != i){
						friends.add(players.get(i).getUsername());
					}
				}
				writers.get(i).println(ResponseContract.startGame(role, friends));
			}else{
				writers.get(i).println(ResponseContract.startGame(role));
			}
		}
	}
	
	//Main LOOP
	public void play() {
		startServer();
		while(isPlay) {
			if(phaseChanged) {
				phase.nextPhase();
				sendBroadcast(ResponseContract.changePhase(phase.getDay(), phase.getTime()));
				phaseChanged = false;
				
				for(int i = 0; i < writers.size(); i ++) {
					if(players.get(i).getIsAlive() == 1) {
						String role = players.get(i).getRole();
						if((role.equals("werewolf") && phase.getTime().equals("night")) || phase.getTime().equals("day")) {
							writers.get(i).println(ResponseContract.voteNotify(phase.getTime()));
						}
			
					}
				}
				checkGameOver();
			}						
		}
		String winner = "";
		if(numWerewolf == 0) {
			winner = "civilian";
		} else if(numCivilian <= numWerewolf) {
			winner = "werewolf";
		}
		//sendBroadcast(ResponseContract.gameOver(winner));
	}
	
	public static void changePhase() {
		phase.nextPhase();
		sendBroadcast(ResponseContract.changePhase(phase.getDay(), phase.getTime()));
	}
	private void checkGameOver() {
		isPlay  = !(numWerewolf == 0 || numWerewolf == numCivilian);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			while (true) {
	         	Socket socket = ss.accept();
	            new ServHandler(socket).start();
	           
	        }
		} catch (Exception e){
			
		}finally {
			try {
				ss.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static int getNumPlayers() {
		return numPlayers;
	}
	public static void setNumPlayers(int num) {
		synchronized(numPlayers) {
			numPlayers = num;
		}
	}
	
	public static void sendBroadcast(String message) {
		for(int i  = 0; i < writers.size(); i++) {
			writers.get(i).write(message);
		}
	}
	
	public static String playersToString() {
		//player to string
		Enumeration<Player> Pl = players.elements();
		String res = "[";
		while(Pl.hasMoreElements()){
			res += Pl.nextElement().toString() + ", ";
		}
		res = res.substring(0,res.length()-2) + "]";
		return res;
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		Server.phase = phase;
	}
}
