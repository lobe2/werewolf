package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Queue;

public class RequestReader extends Thread{
	private Queue<String> request;
	private BufferedReader in;
	
	public RequestReader(Queue<String> request, BufferedReader in) {
		this.request = request;
		this.in = in;
	} 
	public String consume() {
		if(!request.isEmpty()) {
			return request.remove();
		} else {
			return "";
		}
	}
	
	public void add(String S) {
		request.add(S);
	}
	
	@Override
	public void run() {
		try {
			while(true) {
				String req = in.readLine();
				if(req != null && req != "") {
					add(req);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
