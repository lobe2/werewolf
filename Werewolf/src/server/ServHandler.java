package server;

import java.io.*;
import java.net.Socket;
import org.json.JSONObject;
import entity.Player;

public class ServHandler extends Thread {
	private String username;
	private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private int playerId;
    private int my_id;
    public ServHandler(Socket socket) {
    	this.setSocket(socket);
    	try {
    		
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			out = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Override
    public void run() {
   	
		try {
			while (true) {
				
				String input = in.readLine();
                System.out.println("Menerima request ");
				System.out.println(input);
				JSONObject obj = new JSONObject(input);
				String method = obj.getString("method");
				if(method.equals("join")) {
					String name = obj.getString("username");
					if (name == null || name == "" ) {
						out.println(ResponseContract.errorResponse("wrong request"));
		                return;
		            }else if(Server.getNumPlayers() > 6) {
		            	out.println(ResponseContract.failResponse("please wait, game is currently running"));
		            }
					boolean found = false;
		            synchronized (Server.players) {
		            	int k = 0;
		            	found = false;
		            	while(k < Server.getNumPlayers() && !found) {
		            		found = Server.players.get(k).getUsername().equals(name);
		            		k++;
		            	}
		            	
		            	if (!found) {
		            		
		            		int id = Server.getNumPlayers();
		                	int alive = 1;
		                	String IPaddr = obj.getString("ip_address");
		                	int port = obj.getInt("udp_port");
		                	Player P = new Player(id, IPaddr, port, name, alive);
		                    Server.players.put(id, P);
		                    my_id = id;
		                    username = name;
		                    playerId = id;
           		                       	                        
		                    Server.writers.add(out);
		                    out.println(ResponseContract.okJoin(id));
		                    
		                    Server.setNumPlayers(id+1);	 
		                } else {
		                	out.println(ResponseContract.failResponse("username already exist"));
		                	
		                }
		            }
				} else if (method.equals("leave")) {
					Server.players.get(new Integer(playerId)).setIsAlive(0);
				}else if (method.equals("ready")) {
					int num;
					synchronized (Server.players) {
						num = Server.getNumPlayers();
						if( num < 6 ) {
							out.println(ResponseContract.okReady("waiting for other player to start"));
						} else{
							out.println(ResponseContract.okReady("Let's Start"));
							Server.start = true;
						}
					}
				}else if (method.equals("client_address")) {
					out.println(ResponseContract.getAllPlayerInfo());
				} else if (method.equals("accepted_proposal")){
					synchronized (Server.kpu) {
						Server.kpu.setId(obj.getInt("kpu_id"));
						int cnt = Server.kpu.getNum_vote();
						Server.kpu.setNum_vote(cnt+1);
					}
					out.println(ResponseContract.okGeneral(""));
				} else if (method.equals("vote_result_werewolf")) {
					if(Server.kpu.getId() == my_id) {
						if(obj.getInt("vote_status") == -1) {
							out.println(ResponseContract.okGeneral("none killed"));
						} else if(obj.getInt("vote_status") == 1) {
							int idKilled = obj.getInt("player_killed");
							Server.players.get(idKilled).setIsAlive(0);
							out.println(ResponseContract.okGeneral("player " + (new Integer(idKilled).toString() + " was killed")));
							Server.sendBroadcast(ResponseContract.playerKilled(idKilled, Server.players.get(idKilled).getRole()));
							Server.numCivilian--;
							
							synchronized(Server.phaseChanged) {
								Server.phaseChanged = true;
							}
						}
					}
				} else if (method.equals("vote_result_civilian")) {
					if(Server.kpu.getId() == my_id) {
						if(obj.getInt("vote_status") == -1) {
							out.println(ResponseContract.okGeneral("none killed"));
						} else if(obj.getInt("vote_status") == 1) {
							int idKilled = obj.getInt("player_killed");
							Server.players.get(idKilled).setIsAlive(0);
							
							if(Server.players.get(idKilled).getRole().equals("civilian")) {
								Server.numCivilian--;
							} else {
								Server.numWerewolf--;
							}
							
							out.println(ResponseContract.okGeneral("player " + (new Integer(idKilled).toString() + " was killed, player is " + Server.players.get(idKilled).getRole())));
							Server.sendBroadcast(ResponseContract.playerKilled(idKilled, Server.players.get(idKilled).getRole()));
							synchronized(Server.phaseChanged) {
								Server.phaseChanged = true;
							}
						}
					}
				}			
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getPlayerID() {
		return playerId;
	}

	public void setPlayerID(int playerID) {
		this.playerId = playerID;
	}

	public int getMyId() {
		return my_id;
	}

	public void setMyId(int my_id) {
		this.my_id = my_id;
	}
    
}
