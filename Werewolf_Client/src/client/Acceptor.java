package client;

import java.io.*;
import java.net.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import message.Message;

public class Acceptor extends Thread {

	private int previous_proposal_number = -1;
	private int previous_player_id;
	private int accepted_kpu_id;
	private int accepted_proposal_number = -1;
	private int accepted_player_id;
		
	public Acceptor(){
		
	}
	
	public void acceptedProposal() throws Exception{
		String sentence = Message.acceptedProposal(accepted_kpu_id);
		Client.out.printf(sentence);
	}
	
	public void prepareProposalResponse(String input) throws IOException{
		int proposal_number;
		int player_id = 0;
		String response = null;
		try	{
			JSONObject obj = new JSONObject(input);
			String method = obj.getString("method");
			JSONArray proposal_id = obj.getJSONArray("proposal_id") ;
			proposal_number = proposal_id.getInt(0);
			player_id = proposal_id.getInt(1);
			if (previous_proposal_number == -1) {
				previous_proposal_number = proposal_number;
				previous_player_id = player_id;
				response = Message.okResponseDescription("accepted"); 
			} else {
				response = Message.okResponsePreviousId("accepted",previous_proposal_number);
				if (proposal_number > previous_proposal_number){
					previous_proposal_number = proposal_number;
					previous_player_id = player_id;
				} else if (player_id > previous_player_id){
					previous_proposal_number = proposal_number;
					previous_player_id = player_id;
				}
			}
		}catch(InputMismatchException e1)
		{
			response = Message.failResponse("rejected");
		}catch(JSONException e2)
		{
			response = Message.errorResponse("JSON is Incorrect");
		}
		
		String targetAddress = Client.players.get(player_id).getIPAddr();
		InetAddress IPAddress = InetAddress.getByName(targetAddress);
		int targetPort = Client.players.get(player_id).getPort();
		DatagramSocket datagramSocket = new DatagramSocket();
		UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
		byte[] sendData = response.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
		unreliableSender.send(sendPacket);
		datagramSocket.close();
	}
	
	public void acceptProposalResponse(String input) throws IOException{
		int proposal_number;
		int player_id = 0;
		int kpu_id;
		String response = null;
		try	{
			JSONObject obj = new JSONObject(input);
			String method = obj.getString("method");
			JSONArray proposal_id = obj.getJSONArray("proposal_id");
			proposal_number = proposal_id.getInt(0);
			player_id = proposal_id.getInt(1);
			kpu_id = obj.getInt("kpu_id");
			response = Message.okResponseDescription("accepted");
			if(kpu_id > accepted_kpu_id){
				accepted_kpu_id = kpu_id;
				accepted_proposal_number = proposal_number;
				accepted_player_id = player_id;
			}
		}catch(InputMismatchException e1)
		{
			response = Message.failResponse("rejected");
		}catch(JSONException e2)
		{
			response = Message.errorResponse("JSON is Incorrect");
		}
		
		String targetAddress = Client.players.get(player_id).getIPAddr();
		InetAddress IPAddress = InetAddress.getByName(targetAddress);
		int targetPort = Client.players.get(player_id).getPort();
		DatagramSocket datagramSocket = new DatagramSocket();
		UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
		byte[] sendData = response.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
		unreliableSender.send(sendPacket);
		datagramSocket.close();
	}
	
	public int getAcceptedKpuId(){
		return accepted_kpu_id;
	}
	
	public void setAcceptedKpuId(int nKpu_id){
		accepted_kpu_id = nKpu_id;
	}
	
	public int getAcceptedProposalNumber(){
		return accepted_proposal_number;
	}
	
	public int getAcceptedPlayerId(){
		return accepted_player_id;
	}
	
	public void setAcceptedProposalNumber(int n_accepted_proposal_number){
		accepted_proposal_number = n_accepted_proposal_number;
	}
	
	public void setAccepetedPlayerId(int n_accepted_player_id){
		accepted_player_id = n_accepted_player_id;
	}
	
	public int getPreviousProposalNumber(){
		return previous_proposal_number;
	}
	
	public int getPreviousPlayerId(){
		return previous_player_id;
	}
	
	public void setPreviousProposalNumber(int n_previous_proposal_number){
		previous_proposal_number = n_previous_proposal_number;
	}
	
	public void getPreviousPlayerId(int n_previous_player_id){
		previous_player_id = n_previous_player_id;
	}
}

