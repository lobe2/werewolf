package client;
import entity.Player;
import entity.Proposal;
import message.Message;

import java.io.IOException;
import java.net.*;
import java.util.*;

import org.json.JSONObject;

public class Proposer {
	private Hashtable<Integer, Player> acceptor;
	private int lastProposalId;
	private Proposal proposal;
	public static ArrayList<Integer> acceptedId;
	public static Hashtable<Integer, Integer> nAccepted;
	public static int median;
	
	public Proposer(){
		lastProposalId=0;
		median = 3;
	}
	
	public Proposer(Hashtable<Integer, Player> Players){
		setAcceptor(Players);
		lastProposalId=0;
		median = 3;
	}

	public void createProposal(){
		proposal = new Proposal(lastProposalId+1, Client.identity.getPlayerId());
	}
	
	public void sendMessage(String targetAddress, int targetPort, String sentence) throws Exception{
		InetAddress IPAddress = InetAddress.getByName(targetAddress);
		DatagramSocket datagramSocket = new DatagramSocket();
		UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
		byte[] sendData = sentence.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
		unreliableSender.send(sendPacket);
		datagramSocket.close();
	}
	
	public void prepareProposal() throws Exception{
		createProposal();
		Enumeration <Player> player_enum = acceptor.elements();
		while(player_enum.hasMoreElements()){
			Player currentPlayer=player_enum.nextElement();
			String targetAddress = currentPlayer.getIPAddr();
			int targetPort = currentPlayer.getPort();
			String sentence = Message.sendProposal(proposal.getProposalId(), proposal.getProposerId());
			
			sendMessage(targetAddress, targetPort, sentence);
			
			DatagramSocket serverSocket = new DatagramSocket(Client.identity.getPort());
			byte[] receiveData = new byte[1024];	
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			serverSocket.receive(receivePacket);
			String response = new String(receivePacket.getData(), 0, receivePacket.getLength());
			serverSocket.close();
			JSONObject obj = new JSONObject(response);
			String status = obj.getString("status");
			if(status.equals("ok")){
				String description = obj.getString("description");
				int lastKPUAcc = obj.optInt("previous_accepted",-1);
				if (lastKPUAcc!=-1){
					acceptedId.add(lastKPUAcc);
					if(nAccepted.getOrDefault(lastKPUAcc, -1)==-1){
						nAccepted.put(lastKPUAcc, 1);
					} else{
						int count = nAccepted.get(lastKPUAcc);
						nAccepted.replace(lastKPUAcc, count+1);
					}
				}
			} else if(status.equals("fail")){
				String description = obj.getString("description");
			} else if(status.equals("error")){
				String description = obj.getString("description");
			}
		}
		for(int i =0; i< acceptedId.size(); i++){
			int id = acceptedId.get(i);
			if(nAccepted.get(id)>median){
				acceptProposal(proposal,id);
			}
		}
	}
	
	public void acceptProposal(Proposal prop, int kpu_id) throws Exception{
		Enumeration <Player> player_enum = acceptor.elements();
		while(player_enum.hasMoreElements()){
			Player currentPlayer=player_enum.nextElement();
			if(currentPlayer.getIsAlive()==1){
				String targetAddress = currentPlayer.getIPAddr();
				InetAddress IPAddress = InetAddress.getByName(targetAddress);
				int targetPort = currentPlayer.getPort();
				DatagramSocket datagramSocket = new DatagramSocket();
				UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
				String sentence = Message.acceptProposal(prop.getProposalId(),prop.getProposerId(), kpu_id);
				byte[] sendData = sentence.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
				unreliableSender.send(sendPacket);
				datagramSocket.close();
			}
		}
	}
	
	public void setAcceptor(Hashtable<Integer, Player> acceptor){
		this.acceptor= acceptor;
	}
	public void setLastProposalId(int lastProposalId){
		this.lastProposalId= lastProposalId;
	}
	
	public Proposal getProposal(){
		return proposal;
	}
	
	public int getLastProposalId(){
		return lastProposalId;
	}
	
	public int getId(){
		return Client.identity.getPlayerId();
	}	
}
