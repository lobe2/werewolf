package client;

import java.io.*;
import java.net.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

import entity.Player;
import message.Message;

public class Client extends Thread{
	public static int defID=7;
	public static Player identity;
	public static Hashtable<Integer, Player> players;
	public static ArrayList<Integer> listPlayerId;
	
	public static int nCivilian;
	public static int nWerewolf;
	
	private int KPUId;
	private String time;
	private int days;
	private Scanner scan;
	private String friends;
	
	public static String serverAddress;
	public static int serverPort;
	
	private Socket socket;
	public static DatagramSocket UDPsocket;
	public static PrintWriter out;
	public static BufferedReader in;
	
	
	
	private Thread serverHandler;

	public static ArrayList<ClientHandler> CHandler;
	public Client(String address, int port ){
		players = new Hashtable<Integer, Player>();
		listPlayerId =  new ArrayList<Integer>();
		CHandler = new ArrayList<ClientHandler>();
		serverAddress = address;
		serverPort = port;
		nCivilian = 4;
		nWerewolf = 2;
	}
	
	public static void setIdentity(Player iden){
		identity = iden;
	}
	public void connectToServer() throws Throwable, Exception {
		socket = new Socket(serverAddress, serverPort);
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    try {
			out = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void join(String username, String udp_address, int udp_port){
		out.println(Message.joinGame(username, udp_address, udp_port));
		String input;
		try {
			input = in.readLine();
			System.out.println(input);
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) { //status ok
    			int playerId = obj.getInt("player_id");
    			identity = new Player(username,1,playerId);
    			identity.setPlayerId(playerId);
    			if(readyUp()){
        			this.start();
    			} else{
    				System.out.println("tidak siap");
    			}
            } else if(status.equals("fail")){ //status fail
            	String description = obj.getString("description");
            	if(description.equals("user exist")){
            		username = "minta input dr keyboard";
            		join(username, udp_address, udp_port);
            	} else if (description.equals("please wait, game is currently running")){
            		
            	}
            } else { //status error
            	
            }			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void leave(){
		out.println(Message.leaveGame());
		String input;
		try {
			input = in.readLine();
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			identity.setPlayerId(0);
            } else if(status.equals("fail")){//status fail
            	String description = obj.getString("description");
            	
            } else {//status error
            	String description = obj.getString("description");
            	
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean readyUp(){
		out.println(Message.readyUp());
		System.out.println("kirim readyUp");
		String input;
		try {
			input = in.readLine();
			System.out.println(input);
			JSONObject obj = new JSONObject(input);
    		String status = obj.optString("status",null);
    		if(status!=null){
	    		if(status.equals("ok") || status.equals("start")) {
	    			String description = obj.optString("description","");
	    			return true;
	            } else if(status.equals("fail")){//status fail
	            	//String description = obj.getString("description");
	            	return false;
	            } else if(status.equals("error")){//status error
	            	String description = obj.getString("description");   	
	            	return false;
	            } else {
	            	return true;
	            }
    		} else {
    			return true;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static void listClient(){
		out.println(Message.listClient());
		System.out.println(Message.listClient());
		String input;
		try {
			input = in.readLine();
			System.out.println(input);
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			players.clear();
    			listPlayerId.clear();
    			JSONArray clients = obj.getJSONArray("clients");
    			for(int i=0; i<clients.length(); i++){
    				JSONObject client = clients.getJSONObject(i);
    				
    				String IP=client.getString("address");
    				String uname = client.getString("username");
    				int alive = client.getInt("is_alive");
    				int id=client.getInt("player_id");
    				int port= client.getInt("port");
    						
    				Player player = new Player(IP,uname,alive,id,port);
    				players.put(i,player);
    				listPlayerId.add(i);
    			}
    			String descriprion = obj.getString("description");
            } else if(status.equals("fail")){//status fail
            	String description = obj.getString("description");
            	
            } else {//status error
            	String description = obj.getString("description");
            	
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void killWerewolfVote(int playerId) throws Exception{
		String targetAddress = players.get(KPUId).getIPAddr();
		InetAddress IPAddress = InetAddress.getByName(targetAddress);
		int targetPort = players.get(KPUId).getPort();
		DatagramSocket datagramSocket = new DatagramSocket();
		UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
		String sentence = Message.voteWerewolf(playerId);
		byte[] sendData = sentence.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
		unreliableSender.send(sendPacket);
		datagramSocket.close();
		String input;
		try {
			input = in.readLine();
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			String descriprion = obj.getString("description");
    		} else if(status.equals("fail")){//status fail
            	String description = obj.getString("description");	
            } else {//status error
            	String description = obj.getString("description");
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void infoWerewolfKilled(int voteStatus, int playerKilled, Hashtable <Integer,Integer> voteResult){	
		out.println(Message.nightKill(voteStatus, playerKilled, voteResult));
		String input;
		try {
			input = in.readLine();
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			String description = obj.getString("description");
    			
            } else if(status.equals("fail")){//status fail
            	//String description = obj.getString("description");
            	
            } else {//status error
            	String description = obj.getString("description");
            	
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void killCivilianVote(int playerId) throws Exception{
		String targetAddress = players.get(KPUId).getIPAddr();
		InetAddress IPAddress = InetAddress.getByName(targetAddress);
		int targetPort = players.get(KPUId).getPort();
		DatagramSocket datagramSocket = new DatagramSocket();
		UnreliableSender unreliableSender = new UnreliableSender(datagramSocket);
		String sentence = Message.voteCivilian(playerId);
		byte[] sendData = sentence.getBytes();
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, targetPort);
		unreliableSender.send(sendPacket);
		datagramSocket.close();
		String input;
		try {
			input = in.readLine();
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			String descriprion = obj.getString("description");
    		} else if(status.equals("fail")){//status fail
            	String description = obj.getString("description");	
            } else {//status error
            	String description = obj.getString("description");
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void infoCivilianKilled(int voteStatus, int playerKilled, Hashtable <Integer,Integer> voteResult){	
		out.println(Message.dayKill(voteStatus, playerKilled, voteResult));
		String input;
		try {
			input = in.readLine();
			JSONObject obj = new JSONObject(input);
    		String status = obj.getString("status");
    		if(status.equals("ok")) {
    			String description = obj.getString("description");
    			
            } else if(status.equals("fail")){//status fail
            	//String description = obj.getString("description");
            	
            } else {//status error
            	String description = obj.getString("description");
            	
            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public void run(){
		for(;;){
			String input;
			try {
				input = in.readLine();
				System.out.println(input);
				JSONObject obj = new JSONObject(input);
				String method = obj.optString("method", "def");
				if(!method.equals("def")){
					if(method.equals("start")){
						listClient();
						for(int i = 0; i< listPlayerId.size(); i++ ){
							int clientPort =players.get(listPlayerId.get(i)).getPort();
							ClientHandler CHand = new ClientHandler(clientPort);
							CHandler.add(i,CHand);
							CHandler.get(i).run();
						}
						time = obj.getString("time");
						String role = obj.getString("role");
						identity.setRole(role);
						if(role.equals("werewolf")){
							String friends = obj.getString("friend");							
						}
						String description = obj.getString("description");
						String response = Message.okResponse();
						out.println(response);
					} else if(method.equals("change_phase")){
						time = obj.getString("time");
						int nowDays = obj.getInt("days");
						String description = obj.getString("description");
						System.out.println("Waktu berganti menjadi " + time);
						System.out.println("Hari ke : " + nowDays);
						if(nowDays!=days){
							days = nowDays;
							Enumeration<Player> playerEnum= players.elements();
							int max1 = -1;
							int max2 = -2;
							while(playerEnum.hasMoreElements()){
								Player now = playerEnum.nextElement();
								if(now.getIsAlive()==1){
									if(now.getPlayerId()>max1){
										max1 = now.getPlayerId();
									} else if(now.getPlayerId()>max2){
										max2 = now.getPlayerId();
									}
								}
							}
							if(identity.getPlayerId()>= max2){
								Proposer pro = new Proposer(players);
								try {
									pro.prepareProposal();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}	
						}
					} else if(method.equals("vote_now")){
						time = obj.getString("phase");
						listClient();
						System.out.println("list pemain :");
						Enumeration<Player> enumPlayer = players.elements();
						while(enumPlayer.hasMoreElements()){
							Player currPlayer = enumPlayer.nextElement();
							String alive;
							if(currPlayer.getIsAlive()==0){
								alive = "hidup";
							} else {
								alive = "mati";
							}
							System.out.println(currPlayer.getPlayerId() +" : "+currPlayer.getUsername()+ " : "+alive);
						}
						if(time.equals("day")){
							scan = new Scanner(System.in);
							System.out.println("---- VOTING SIANG ----");
							System.out.println("input id user yang ingin dibunuh");
							int id = scan.nextInt();
							try {
								killCivilianVote(id);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if(time.equals("night")){
							System.out.println("temenmu sesama srigala bejat: "+friends);
							scan = new Scanner(System.in);
							System.out.println("---- VOTING MALAM ----");
							System.out.println("input id user yang ingin dibunuh");
							int id;
							String playerRole;
							do{
								id = scan.nextInt();
								playerRole = players.get(id).getRole();
								if(id == identity.getPlayerId()){
									System.out.println("masukkan id orang lain !");
								} else if(playerRole.equals("werewolf")){
									System.out.println("jangan masukkam id punya werewolf !");
								}
							}while(id == identity.getPlayerId()|| playerRole.equals("werewolf"));
							try {
								killWerewolfVote(id);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else if(method.equals("game_over")){
						String winner = obj.getString("winner");
						String description = obj.getString("description");
						String msg = Message.okResponse();
						out.printf(msg);
						System.out.println("----GAME OVER----");
						System.out.println("winner : " + winner);
						System.out.println(description);
					} else if (method.equals("player_killed")){
						int id = obj.getInt("id");
						String role = obj.getString("role");
						if (identity.getPlayerId()==id){
							identity.setIsAlive(0);
							leave();
							System.out.println("Kamu terbunuh. haha");
						} else {
							Player p = players.get(id);
							p.setIsAlive(0);
							players.replace(id, p);
							System.out.println("player dengan ID = "+id+" terbunuh");
						}
						if(role.equals("werewolf")){
							nWerewolf --;
						} else if(role.equals("civilian")){
							nCivilian --;
						}
					} else if(method.equals("KPU_voted")){
						KPUId = obj.getInt("kpu_id");
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void start(){
		if(serverHandler==null){
			serverHandler = new Thread(this, "serverHandler");
			serverHandler.start();
		}
	}
	
	public Player getIdentity(){
		return identity;
	}
	
	public static void main(String args[]) throws Exception
	{
		Scanner scan = new Scanner(System.in);

		System.out.println("======== Welcome to Werewolf ========");
		System.out.println("Masukkan Alamat Server : ");
		//String serverAddress = scan.next();
		String serverAddress = "localhost";
		int serverPort = 9876;
		//connect to server
		Client C= new Client(serverAddress,serverPort);
		try {
			C.connectToServer();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//prepare to join
		
		System.out.println("Masukkan Port Player : ");
		int playerPort = scan.nextInt();
		Player pp = new Player();
		pp.setPort(playerPort);
		C.UDPsocket = new DatagramSocket(playerPort);
		C.UDPsocket.setReuseAddress(true);
		//System.out.println("Masukkan Username Player : ");
		String username = new Integer(playerPort).toString();
		pp.setUsername(username);
		String state = "";
		InetAddress playerAddress = InetAddress.getLocalHost();
		pp.setIPAddr(playerAddress.getHostAddress());
		C.setIdentity(pp);
	
		//join game
		C.join(C.getIdentity().getUsername(),C.getIdentity().getIPAddr(),C.getIdentity().getPort());
		//ready up
	}
}
