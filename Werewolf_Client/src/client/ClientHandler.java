package client;

import java.net.DatagramSocket;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONObject;

import message.Message;

public class ClientHandler extends Thread{
	private int port;
	private Acceptor acc;
	Hashtable<Integer, Integer> vote;
	int countVotes;
	ArrayList<Integer> playerVotes; 
	private Thread CHandler;
	
	public ClientHandler(int port){
		this.port = port;
		countVotes = 0;
		playerVotes = new ArrayList(6);
		acc = new Acceptor();
	}
	
	public ClientHandler(){
		this.port = 9009;
		countVotes = 0;
		playerVotes = new ArrayList(6);
		acc = new Acceptor();
	}
	
	public void setPort(int port){
		this.port = port;
	}
	
	public int getPort(){
		return port;
	}
	
	public void run(){
		try {
			byte[] receiveData = new byte[1024];
			while(true)
			{
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				try {
					Client.UDPsocket.receive(receivePacket);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String input = new String(receivePacket.getData(), 0, receivePacket.getLength());
				JSONObject obj = new JSONObject(input);
				String method = obj.optString("method",null);
				if(method != null){
					if(method.equals("prepare_proposal")){
						acc.prepareProposalResponse(input);
					} else if(method.equals("accept_proposal")){
						acc.acceptProposalResponse(input);
					} else if(method.equals("vote_werewolf")){
						int playerId = obj.getInt("player_id");
						int count = vote.getOrDefault(playerId, 0);
						vote.replace(playerId, count+1);
						playerVotes.set(playerId, count+1);
						countVotes++;
						if (countVotes == Client.nCivilian){
							//cari mayoritas
							int nMax=0;
							int idxMax=0;
							int Max=0;
							int i=0;
							while(i<6 && nMax<1){
								if(playerVotes.get(i)!=0){
									nMax++;
									Max = playerVotes.get(i);
									idxMax = i;
								} else i++;
							}
							if(i<5){
								for(int j = i+1 ; j<6; j++){
									if(playerVotes.get(j)>Max){
										Max = playerVotes.get(j);
										idxMax = j;
										nMax ++;
									}
								}
							}
							if(nMax < 1 || nMax>1){
								Client.infoWerewolfKilled(-1, -1, vote);
							} else {
								Client.infoWerewolfKilled(1, idxMax, vote);
							}
							countVotes=0;
							vote.clear();
							playerVotes.clear();
						}	
					} else if(method.equals("vote_civilian")){
						int playerId = obj.getInt("player_id");
						int count = vote.getOrDefault(playerId, 0);
						vote.replace(playerId, count+1);
						playerVotes.set(playerId, count+1);
						countVotes++;
						if (countVotes == Client.nCivilian){
							//cari mayoritas
							int nMax=0;
							int idxMax=0;
							int Max=0;
							int i=0;
							while(i<6 && nMax<1){
								if(playerVotes.get(i)!=0){
									nMax++;
									Max = playerVotes.get(i);
									idxMax = i;
								} else i++;
							}
							if(i<5){
								for(int j = i+1 ; j<6; j++){
									if(playerVotes.get(j)>Max){
										Max = playerVotes.get(j);
										idxMax = j;
										nMax ++;
									}
								}
							}
							if(nMax < 1 || nMax>1){
								Client.infoCivilianKilled(-1, -1, vote);
							} else {
								Client.infoCivilianKilled(1, idxMax, vote);
							}
							countVotes=0;
							vote.clear();
							playerVotes.clear();
						}	
					}
				}
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void start(){
		if(CHandler==null){
			CHandler = new Thread(this, "ClientHandler");
			CHandler.start();
		}
	}
}

