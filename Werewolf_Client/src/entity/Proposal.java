package entity;

public class Proposal{
	private int proposalId;
	private int proposerId;
	//private String status;
	
	public Proposal(int porposalId, int porposerId){
		setProposalId(proposalId);
		setProposerId(proposerId);
	}
	
	public void setProposalId(int proposalId){
		this.proposalId= proposalId;
	}
		
	public void setProposerId(int proposerId){
		this.proposerId= proposerId;
	}
	
	public int getProposalId(){
		return proposalId;
	} 
	
	public int getProposerId(){
		return proposerId;
	}
	
	public String compare(Proposal P){
		int a = this.proposalId;
		int b = this.proposerId;
		int c = P.proposalId;
		int d = P.proposerId;
		if((a > c) ||( a==c && b>d )){
			return "higher";
		} else if((a > c) ||( a==c && b<d )){
			return "lower";
		} else if(a==c && b==d){
			return "same";
		} else {
			return "error";
		}
	}
}