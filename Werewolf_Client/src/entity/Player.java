package entity;
/**
 * Nama : Muhtar H
 * NIM : 13513068
 * */
public class Player {
	private String IPAddr;
	private String username;
	private int isAlive;
	private int playerId;
	private int port;
	private String role;
	
	public Player() {
		setIPAddr("0.0.0.0");
		setUsername("undef");
		setIsAlive(1);
		setPlayerId(-1);
		setPort(9007);
	}
	
	public Player(String IP, String uname, int alive, int id, int port) {
		setIPAddr(IP);
		setUsername(uname);
		setIsAlive(alive);
		setPlayerId(id);
		setPort(port);
	}
	
	public Player(String uname, int alive, int id){
		setIPAddr("0.0.0.0");
		setUsername(uname);
		setIsAlive(alive);
		setPlayerId(id);
		setPort(9007);
	}
	
	
	/**
	 * GETTER and SETTER
	 * */
	public String getIPAddr() {
		return IPAddr;
	}
	public void setIPAddr(String iPAddr) {
		IPAddr = iPAddr;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	public int getPort() {
		return port;
	}
	public int getIsAlive() {
		return isAlive;
	}
	public void setIsAlive(int isAlive) {
		this.isAlive = isAlive;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}	
	
	public void setRole(String role){
		this.role = role;
	}
	
	public String getRole(){
		return role;
	}
}
