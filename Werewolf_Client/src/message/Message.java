package message;

import java.util.Enumeration;
import java.util.Hashtable;

public class Message {
	private static final String joinGame = "\"method\" : \"join\"";
	private static final String leaveGame = "\"method\" : \"leave\"";
	private static final String readyUp = "\"method\" : \"ready\"";
	private static final String listClient = "\"method\" : \"client_address\"";
	private static final String sendProposal = "\"method\" : \"prepare_proposal\"";
	private static final String acceptProposal = "\"method\" : \"accept_proposal\"";
	private static final String acceptedProposal = "\"method\" : \"accepted_proposal\"";
	private static final String voteResult = "\"method\" : \"vote_result\"";
	private static final String voteWerewolf = "\"method\" : \"vote_werewolf\"";
	private static final String nightKill = "\"method\" : \"vote_result_werewolf\"";
	private static final String voteCivilian = "\"method\" : \"vote_civilian\"";
	private static final String dayKill = "\"method\" : \"vote_result_civilian\"";
		
	private static final String failMethod = "\"status\" : \"fail\"";
	private static final String okMethod = "\"status\" : \"ok\"";
	private static final String errorMethod = "\"status\" : \"error\"";
	
	//response
	
	public static String okResponse() {
		String response = "{" + okMethod + "\"}";
		return response;
	}
	
	public static String okResponseDescription(String description) {
		String response = "{" + okMethod + "," +
				"\"description\" : \"" + 
				description + "\"}";
		return response;
	}
	
	public static String failResponse(String description) {
		String response = "{" + failMethod + "," +
					"\"description\" : \"" + 
					description + "\"}";
		return response;
	}
	
	public static String errorResponse(String description) {
		String response = "{" + errorMethod + "," +
					"\"description\" : \"" + 
					description + "\"}";
		return response;
	}
	
	public static String okResponsePreviousId(String description, int previous_accepted) {
		String response = "{" + okMethod + "," +
				"\"description\" : \"" + 
				description +  "," +
				"\"previous_accepted\" : \"" + 
				previous_accepted + "\"}";
		return response;
	}
	
	//request
	
	public static String joinGame(String username, String udp_address, int udp_port){
		String response = "{" + joinGame + "," +
				"\"username\" : \"" + 
				username + "\"," +
				"\"ip_address\" : \"" + 
				udp_address + "\"," +
				"\"udp_port\" : " + 
				udp_port + "}";
		return response;
	}
	
	public static String readyUp(){
		String S = "{" + readyUp + "}";
		return S;
	}
	
	public static String listClient(){
		String S = "{" + listClient + "}";
		return S;
	}
	
	public static String leaveGame(){
		String S = "{" + leaveGame + "}";
		return S;
	}
	
	public static String sendProposal (int proposal_id, int proposer_id){
		String S = "{" + sendProposal + "," + 
				"\"proposal_id\" : [" + 
				proposal_id + "," +
				proposer_id +
				"]\"}";
		return S;
	}
	
	public static String acceptProposal (int proposal_id, int proposer_id, int kpu_id){
		String S = "{" + acceptProposal + "," + 
					"\"proposal_id : [" + 
					proposal_id + "," +
					proposer_id + "]," + 
					"\"kpu_id : " +
					kpu_id + "}";
		return S;
	}
	
	public static String acceptedProposal (int kpu_id){
		String S = "{" + acceptedProposal + "," +
					"\"kpu_id\" : " +
					kpu_id + 
					"\"description\" : \"kpu is selected\"}";
		return S;
	}
	
	
	public static String voteWerewolf (int player_id){
		String S = "{" + voteWerewolf + "," +
				"\"player_id\" : " + 
				player_id + "}";
		return S;
	}
	
	public static String nightKill (int vote_status, int player_killed, Hashtable<Integer, Integer> vote_result){
		String S = "";
		if (vote_status > 0) {
			S += "{" + nightKill + "," +
				"\"vote_status\" : " +
				vote_status + "," +
				"\"player_killed\" :" + 
				player_killed + ",";
		} else {
			S += "{" + voteResult + "," +
					"\"vote_status\" : " +
					vote_status + ",";
		}
			
		S +=	"\"vote_result\" : [";
		
		Enumeration<Integer> votes= vote_result.elements();
		int n=0;
		while(votes.hasMoreElements()){
			votes.nextElement();
			int vote = vote_result.getOrDefault(n, -1);
			if(vote==-1){
				//nothing
			} else {
				S+="[" + n + "," + vote+ "]";
			}
			n++;
		}
		
		S += "]}";
		return S;
	}
	
	public static String voteCivilian (int player_id){
		String S = "{" + voteCivilian + "," +
				"\"player_id\" : " + 
				player_id + "}";
		return S;
	}
	
	public static String dayKill (int vote_status, int player_killed, Hashtable<Integer, Integer> vote_result){
		String S = "";
		if (vote_status > 0) {
			S += "{" + dayKill + "," +
				"\"vote_status\" : " +
				vote_status + "," +
				"\"player_killed\" :" + 
				player_killed + ",";
		} else {
			S += "{" + voteResult + "," +
					"\"vote_status\" : " +
					vote_status + ",";
		}
			
		S +=	"\"vote_result\" : [";
			
		Enumeration<Integer> votes= vote_result.elements();
			int n=0;
			while(votes.hasMoreElements()){
				votes.nextElement();
				int vote = vote_result.getOrDefault(n, -1);
				if(vote==-1){
					//nothing
				} else {
					S+="[" + n + "," + vote+ "]";
				}
				n++;
			}	
		
		S += "]}";
		return S;
	}
}	
